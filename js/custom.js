(function($) {
  Drupal.behaviors.homePageSettings = {
    attach: function() {
      $('#slider1').nivoSlider();
      if (Drupal.settings.nodeType === 'homepage_fila') {
        $('#sec1slider').nivoSlider({
          effect: Drupal.settings.slider1Settings.effect, // Specify sets like: 'fold,fade,sliceDown'
          slices: parseInt(Drupal.settings.slider1Settings.slices, 10), // For slice animations
          animSpeed: parseInt(Drupal.settings.slider1Settings.speed, 10), // Slide transition speed
          pauseTime: parseInt(Drupal.settings.slider1Settings.pauseTime, 10), // How long each slide will show
          startSlide: parseInt(Drupal.settings.slider1Settings.startSlide, 10), // Set starting Slide (0 index)
        });
      }
      $('#slider2').nivoSlider();
      var imgLoad = imagesLoaded($('.mainContentRegion'));

      function onAlways(instance) {
        var width = 0,
          height = 0;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) || $(window).width() <= 960) {
          $.getScript('../libraries/jQmobile/jQmobile.js');
          var left_pos = 0;
          var count = $('.sec2-inner').find('.sec-item').length;
          $('.sec2-inner').css({
            'width': (count * $(window).width()) + 'px'
          });
          $('.sec2-inner').find('.sec-item').each(function() {
            $(this).css({
              'width': $(window).width() + 'px'
            });
            $(this).find('.image-container').css({
              'max-width': '80%',
              'margin': '0 auto',
              'text-align': 'center'
            });
            $(this).find('.detail-container').css({
              'max-width': '80%',
              'margin': '0 auto'
            });
            $(this).find('img').css({
              'max-width': '100%',
              'height': 'auto'
            });
          });
          $('.sec2-inner').on('swipeleft', function(event) {
            $('.sec2').find('.info-wrapper').fadeOut(500);
            if (left_pos !== (0 - ((count - 1) * $(window).width()))) {
              left_pos -= $(window).width();
              $('.sec2-inner').animate({
                'left': left_pos + 'px'
              }, 200);
            }
          });
          $('.sec2-inner').on('swiperight', function(event) {
            $('.sec2').find('.info-wrapper').fadeOut(500);
            if (left_pos < 0) {
              left_pos += $(window).width();
              $('.sec2-inner').animate({
                'left': left_pos + 'px'
              }, 200);
            }
          });
        } else {
          $('.sec2-inner').find('.sec-item').each(function() {
            width += $(this).outerWidth(true);
          });
          $('.sec2-inner').css({
            'width': width + 'px'
          });
          $('.sec2-content').jScrollPane();
        }
        //
        $('.sec2-inner').find('.sec-item').each(function() {
          width += $(this).outerWidth(true);
          var imgWidth = $(this).find('.image-container img').width();
          $(this).find('.image-container').css({
            'width': imgWidth + 'px'
          });
          if (height < $(this).find('.image-container').find('img').height()) {
            height = $(this).find('.image-container').find('img').height();
          }
        });
        $('.sec-item .image-container').css({
          'height': height + 'px'
        }).addClass('images-processed');

        var timer = setInterval(function() {
          if (parseInt($('.sec2').find('.jspPane').css('left'), 10) < 0) {
            $('.sec2').find('.info-wrapper').fadeOut(500);
          }
        }, 100);
        //for sec3
        $('#slider2').find('.nivo-caption').before($('.sec3-inner').find('.nivo-controlNav'));
        //for sec 4
        $('.men-section').find('.explore').click(function() {
          $(this).parents('.men-section').find('.section-items').fadeIn(400);
        });
        $('.men-section').find('.section-items').find('.close-button').click(function() {
          $(this).parents('.men-section').find('.section-items').fadeOut(500);
        });

      }
      // bind with .on()
      imgLoad.on('always', onAlways);
    }
  };
})(jQuery);
