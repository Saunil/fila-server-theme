<div id="sec1slider" class="nivoSlider">
  <?php foreach ($items as $delta => $item): ?>
    <?php hide($item['links']); ?>
    <?php print render($item); ?>
  <?php endforeach; ?>
</div>
