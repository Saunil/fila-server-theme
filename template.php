<?php

/**
  * Implements hook_preprocess_field_slideshow()
  * - Used to rip of the extra inner div that field module gives
  * - Initial code snippet is copied from the field Slideshow module file
  */

function fila_server_preprocess_field_slideshow(&$variables) {
  $slide_theme = (isset($variables['breakpoints']) && isset($variables['breakpoints']['mapping']) && !empty($variables['breakpoints']['mapping'])) ? 'picture' : 'image_style';
  foreach ($variables['items'] as $num => $item) {
    // Generate the image render elements
    $image = array();
    $image['path'] = $item['uri'];
    $image['attributes']['class'] = array('field-slideshow-image', 'field-slideshow-image-' . (1+$num));
    $image['alt'] = isset($item['alt']) ? $item['alt'] : '';
    if (isset($item['width']) && isset($item['height'])) {
      $image['width'] = $item['width'];
      $image['height'] = $item['height'];
    }
    elseif ($item["type"] == 'image') {
      $image_dims = getimagesize($image['path']);
      $image['width'] = $image_dims[0];
      $image['height'] = $image_dims[1];
    }
    else {
      $image = array(
        'width' => 0,
        'height' => 0
      );
    }
    if (isset($item['title']) && drupal_strlen($item['title']) > 0) $image['title'] = $item['title'];
    // Code sniipet to render the inner markup of the field Slideshow
    if (isset($variables['image_style']) && $variables['image_style'] != '') {
      $image['style_name'] = $variables['image_style'];
      $image['breakpoints'] = $variables['breakpoints'];
      $variables['items'][$num]['image'] = theme($slide_theme, $image); // Only rendering the image (Extra inner Div ripped off)
    }
    elseif (isset($variables['file_style']) && $variables['file_style'] != '') {
      $media = file_view((object) $variables['items'][$num], $variables['file_style']);
      $variables['items'][$num]['image'] = render($media); // Only rendering the image (Extra inner Div ripped off)
    }
    else {
      $variables['items'][$num]['image'] = theme('image', $image); // Only rendering the image (Extra inner Div ripped off)
    }
  }
}
function fila_server_preprocess_page(&$vars){
  if(isset($vars['node'])){
    if($vars['node']->type === 'homepage_fila'){
      //Hiding the unncessary details
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_country_homepage']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_sec1_fg_animation_speed']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_sec1_fg_link']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_sec1_fg_pause_time']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_sec1_fg_slices']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_sec1_fg_start_slide']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['field_sec1_fg_transition_effect']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['links']);
      hide($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]['comments']);

      // dsm($vars['page']['content']['system_main']['nodes'][$vars['node']->vid]);
      $vars['theme_hook_suggestions'][] = 'page__node__homepage';
    }
    }
}
function fila_server_preprocess_node(&$vars){
  if($vars['type'] === 'homepage_fila'){
    drupal_add_js(array(
      'slider1Settings' => array(
        'speed'      => $vars['field_sec1_fg_animation_speed'][0]['value'],
        'pauseTime'      => $vars['field_sec1_fg_pause_time'][0]['value'],
        'effect'       => $vars['field_sec1_fg_transition_effect'][0]['value'] ,
        'slices'   => $vars['field_sec1_fg_slices'][0]['value'],
        'startSlide'  => $vars['field_sec1_fg_start_slide'][0]['value']
        )
      )
      ,array('type' => 'setting')
    );
  }
  drupal_add_js(array(
    'nodeType' => $vars['type']
  ),
  array('type' => 'setting'));
}
function fila_server_field_collection_view($variables) {
  $element = $variables['element'];
  return $element['#children'];
}
