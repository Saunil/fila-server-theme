************************************************************************************************************************************
************************************************ Fila Server version 1.x ***********************************************************
************************************************************************************************************************************

 - Fila Server Theme is a Fully responsive theme which uses mobile First Approach. The theme has only a single breakpoint of 960px .

 - The theme uses sass, and compass as CSS preprocessor languages and has a responsive adaptable Grid system.

 - For Desktop, the layout follows 12 grid system with grid width 88px , gutter width 20px and lef and right spacing of 45px and container width of 1366px.

 - For Mobile, the layout follows 4 grid system with grid wdith of 124px , gutter width of 20px and left and right spacing of 42px and container width is 640px

 - Responsive grid layouting has been implemented through Susy.

 - Theme uses Grunt to run sass preprocess task to compile scss to css.

 - Use 'npm install' to install the grunt dependencies.

 - The theme is based on implementing minimalistic html to aheive the standard HTML markup.

 - CamelCases CSS Classes are coming from the Fila theme and are independent of drupal functionality while the classes with 'hyphen' (-) in between are directly coming from the drupal backend are are required by drupal to run in proper manner.

*************************************************************************************************************************************
